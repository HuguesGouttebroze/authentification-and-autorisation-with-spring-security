# Notes Spring Security

	* Nous allons utiliser Spring Security pour sécuriser le formulaire de connexion de cette appli.
	
## Sécuriser l’accès par l’authentification et l'autorisation



## Spring Security ajoute trois niveaux de sécurité : 

	* pare-feu HTTP (empêche les flux suspects de pénétrer dans l'appli.)

	* DelegatingFilterProxy (dirige le reste des flux HTTP vers les filtres de sécurité appropriés)

	* chaîne de filtres sécurisée (héberge les règles de sécurité de l'appli.) 

## Dépendances ajoutées au projet

	* Spring Web
	* Spring Security
	* OAuth 2.0.

