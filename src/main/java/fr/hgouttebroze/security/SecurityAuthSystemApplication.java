package fr.hgouttebroze.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityAuthSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityAuthSystemApplication.class, args);
	}

}
