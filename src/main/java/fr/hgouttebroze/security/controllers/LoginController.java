package fr.hgouttebroze.security.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

	@GetMapping("/user")
	public String getUser() {
		return "WELCOME, USER";
	}
	
	@GetMapping("/admin")
	public String getAdmin() {
		return "WELCOME, ADMIN";
	}
}
